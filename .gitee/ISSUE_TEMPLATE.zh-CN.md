# 请直接在最下方指示中填写，其他模板部分请勿删除！！！

---
name: FriendsLinks
about: 自助提交友链
title: 'FriendsLinks'
labels: 'friends'
assignees: 'Akimoto'

---
# 使用说明书
|  选项   | 描述  |
|  ----  | ----  |
| title  | 网站标题 |
| url  | 主页链接 |
| avatar  | 头像 / Logo |
| description  | 描述 / 个性签名 |
| screenshot  | 主页截图/封面 (建议添加) |

# 在下面填写↓
<!-- 请在双引号中填写 -->
```json
{
    "title": " ",
    "url": " ",
    "avatar": " ",
    "description": " ",
    "screenshot": " "
}
```




