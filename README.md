# FriendsLinks

#### 介绍
自助提交友链，直接提交Issue，返回友链页刷新即可自动生效~

# 请直接在 Issue 按指示填写，其他模板部分请勿删除！！！


# 使用说明书

|  选项   | 描述  |
|  ----  | ----  |
| title  | 网站标题 |
| url  | 主页链接 |
| avatar  | 头像 / Logo |
| description  | 描述 / 个性签名 |
| screenshot  | 主页截图 |

<!-- 请在双引号中填写 -->
```json
{
    "title": "",
    "description": "",
    "screenshot": "",
    "url": "",
    "avatar": ""
}
```

